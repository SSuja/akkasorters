package com.wolf.akka.sorters;

import org.apache.log4j.Logger;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

/**
 * This controller contains an action to handle HTTP requests to the
 * application's home page.
 */
public class TcaMain {
	final static Logger logger = Logger.getLogger(TcaMain.class);

	public static void main(String[] args) {
		final ActorSystem system = ActorSystem.create("Sortingwithakka");
		final ActorRef bubbleSortActor = system.actorOf(BubbleSort.props("selection"), "bubbleSortActor");
		final ActorRef insertionSortActor = system.actorOf(InsertionSort.props("insertion"), "insertionSortActor");
		final ActorRef quickSortActor = system.actorOf(QuickSort.props("quicksort"), "quickSortActor");
		final ActorRef selectionSortActor = system.actorOf(SelectionSort.props("bubble"), "selectionSortActor");

		int[] data1 = new int[50000];
		for (int i = 0; i < data1.length; i++) {
			data1[i] = (int) (Math.random() * 65536);
		}
		int[] data2 = data1.clone();
		int[] data3 = data1.clone();
		int[] data4 = data1.clone();
		long start = System.nanoTime();
		bubbleSortActor.tell(data1, ActorRef.noSender());
		insertionSortActor.tell(data2, ActorRef.noSender());
		quickSortActor.tell(data3, ActorRef.noSender());
		selectionSortActor.tell(data4, ActorRef.noSender());
		long duration = System.nanoTime() - start;
		logger.info("running time = " + duration * 1e-9 + " seconds");
	}

}