package com.wolf.akka.sorters;

import org.apache.log4j.Logger;

import akka.actor.Props;
import akka.actor.UntypedActor;

public class SelectionSort extends UntypedActor {
	final static Logger logger = Logger.getLogger(BubbleSort.class);

	static public Props props(String message) {
		return Props.create(SelectionSort.class);
	}

	static public class Sorting {

		public Sorting(int[] message) {
			int[] num = message;
			int i, j, first, temp;

			logger.info("Starting SelectionSort..");

			for (i = num.length - 1; i > 0; i--) {
				first = 0; // initialise to subscript of first element
				for (j = 1; j <= i; j++) { // locate smallest element between positions 1 and i.
					if (num[j] < num[first])
						first = j;
				}
				temp = num[first]; // swap smallest found with element in position i.
				num[first] = num[i];
				num[i] = temp;
			}
			logger.info("Ending SelectionSort");

		}

	}

	@Override
	public void onReceive(Object message) throws Exception {
		new Sorting((int[]) message);
		getSender().tell("Hello, " + message, getSelf());

	}
}