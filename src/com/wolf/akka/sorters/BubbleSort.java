package com.wolf.akka.sorters;


import org.apache.log4j.Logger;

import akka.actor.Props;
import akka.actor.UntypedActor;

public class BubbleSort extends UntypedActor {
	final static Logger logger = Logger.getLogger(BubbleSort.class);

    static public Props props(String message) {
      return Props.create(BubbleSort.class);
    }
    static public class Sorting {

        public Sorting(int[] message) {
       	   int [] num =message;
        	   int j;
       	    boolean flag = true;   // set flag to true to begin first pass
       	    int temp;   //holding variable
       	    
            logger.info("Starting BubbleSort..");

       	    while ( flag ){
                  flag= false;    //set flag to false awaiting a possible swap
                  for (j=0;  j < num.length -1;  j++){
                     if ( num[j] < num[j+1] ){   // change to > for ascending sort
                         temp = num[j];                //swap elements
                         num[j] = num[j+1];
                         num[j+1] = temp;
                         flag = true;              //shows a swap occurred  
                     } 
                  }
               }
            logger.info("Ending BubbleSort");

        }

		
      }
    
    @Override
    public void onReceive(Object message) throws Exception {
        new Sorting((int[]) message);
        getSender().tell("Hello, " + message, getSelf());
        
    }
}