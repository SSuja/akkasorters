package com.wolf.akka.sorters;

import org.apache.log4j.Logger;

import akka.actor.Props;
import akka.actor.UntypedActor;

public class InsertionSort extends UntypedActor {
	final static Logger logger = Logger.getLogger(BubbleSort.class);

	static public Props props(String message) {
		return Props.create(InsertionSort.class);
	}

	static public class Sorting {

		public Sorting(int[] message) {
			int[] num = message;
			int j; // the number of items sorted so far
			int key; // the item to be inserted
			int i;

			logger.info("Starting InsertionSort..");

			for (j = 1; j < num.length; j++) { // Start with 1 (not 0)
				key = num[j];
				for (i = j - 1; (i >= 0) && (num[i] < key); i--) { // Smaller values are moving up
					num[i + 1] = num[i];
				}
				num[i + 1] = key; // Put the key in its proper location
			}
			logger.info("Ending InsertionSort");

		}

	}

	@Override
	public void onReceive(Object message) throws Exception {
		new Sorting((int[]) message);
		getSender().tell("Hello, " + message, getSelf());

	}
}